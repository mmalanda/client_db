(**
  Module in charge of handling client objects. It uses a functor
  whose argument is a module handling the gender of a client.
*)

(** Module type that the [Gender] module has to match. *)
module type CompIOType =
sig
  type t
  val compare : t -> t -> int
  val to_string : t -> string
  val of_string : string -> t
end

(** Module type for the [Client] module, instantiated by {!Make}. *)
module type C =
sig

  (** Type of the objects representing the clients' genders. *)
  type gender

  (** Type of the objects representing clients. *)
  type t

  (** Exception raised when the provided client information is invalid. *)
  exception WrongClientInformation

  (** [add_client first_name last_name age gender] creates a new client.
    @param first_name The first name of the client.
    @param last_name The last name of the client.
    @param age The age of the client.
    @param gender The string description of the gender of the client.
    @return A new object of type {!t}.
    @raise WrongClientInformation if the provided client information is invalid.
  *)
  val add_client : string -> string -> int -> string -> t

  (** [compare c1 c2] compares two clients.
    @param c1 An object of type {!t}
    @param c2 An object of type {!t}
    @return 0 if [c1] = [c2], -1 if [c1] < [c2], 1 if [c1] > [c2].
  *)
  val compare : t -> t -> int

  (** [to_string c] converts a client to a printable string. 
    @param c An object of type {!t}.
    @return A string describing the client.
  *)
  val to_string : t -> string

  (** [wish_happy_birthday c] adds 1 to the age of client [c] and prints a nice birthday message.
      @param c An object of type {!t}.
  *)
  val wish_happy_birthday : t -> unit
end

(** Functor for module type {!C}.
    @param Gender A module of type {!CompIOType}.
    @return A module of type {!C}.
*)
module Make (Gender : CompIOType) :
  C with type gender = Gender.t