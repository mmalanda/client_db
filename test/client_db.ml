open My_db

let female = Gender.of_string "F"
let male = Gender.of_string "m"
let nonbinary = Gender.of_string "nB"

let test_gender_to_string1 () =
  Alcotest.(check string) "F gender" "woman" (Gender.to_string female)

let test_gender_to_string2 () =
  Alcotest.(check string) "M gender" "man" (Gender.to_string male)

let test_gender_to_string3 () =
  Alcotest.(check string) "NB" "non-binary person" (Gender.to_string nonbinary)

let test_add_client () =
  Alcotest.check_raises "unknown gender"
    Client.WrongClientInformation
    (fun () -> ignore(Client.add_client "John" "Doe" 20 "y"))

let suite1 =
  ["Gender.to_string",
    [
      ("to_string Female", `Quick, test_gender_to_string1);
      ("to_string Male", `Quick, test_gender_to_string2);
      ("to_string Nonbinary", `Quick, test_gender_to_string3);
    ];
  "Client.add_client",
    [
      ("add_client unknown gender", `Quick, test_add_client);
    ];
  ]

let () = Alcotest.run "Gender test suite" suite1