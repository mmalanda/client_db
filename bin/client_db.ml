open My_db

let database = Db.empty
let me = Client.add_client "Mathias" "Malandain" 37 "m"
let database = Db.add me database

let () = Db.iter
  (fun c -> print_endline (Client.to_string c))
  database

let () = Client.wish_happy_birthday me
let () = print_endline (Client.to_string me)
