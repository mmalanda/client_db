(** Interface for the library, exposing module {!module:Gender}
    and instantiating modules {!module:Client} and {!module:Db}.
*)

module Gender : Client.CompIOType
module Client : Client.C with type gender = Gender.t
module Db : Set.S with type elt = Client.t