(** Module declaring a comparable gender type to be used in the {!module:Client} module *)

(** Gender type *)
type t

(** Function comparing two gender objects *)
val compare : t -> t -> int

(**
    Function returning a gender object from a string input.
    @raise Invalid_argument when the provided string is not a valid input
*)
val of_string : string -> t

(** Function converting a gender object to string *)
val to_string : t -> string