type t =
  Female
  | Male
  | Nonbinary

let compare g1 g2 = match (g1,g2) with
  Female, Female
  | Male, Male
  | Nonbinary , Nonbinary -> 0
  | Female, Male
  | Female, Nonbinary
  | Male, Nonbinary -> -1
  | _ , _ -> 1

let of_string gender_str =
  match String.lowercase_ascii gender_str with
    "f" -> Female
    | "m" -> Male
    | "nb" -> Nonbinary
    | (_ as s) -> invalid_arg ("Gender.of_string: " ^ s ^ " is not a valid input")

let to_string = function
  Female -> "woman"
  | Male -> "man"
  | Nonbinary -> "non-binary person"