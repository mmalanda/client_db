(**
   [client_db] is a prototype of a small client database handler.
   (c) Mathias Malandain, 2023.
*)


module type CompIOType =
sig
  type t
  val compare : t -> t -> int
  val to_string : t -> string
  val of_string : string -> t
end

module type C =
sig
  type t
  type gender
  exception WrongClientInformation
  val add_client : string -> string -> int -> string -> t
  val compare : t -> t -> int
  val to_string : t -> string
  val wish_happy_birthday : t -> unit
end

module Make(Gender: CompIOType) =
struct

  exception WrongClientInformation

  type gender = Gender.t

  type t =
    {
      first_name: string;
      last_name: string;
      mutable age: int;
      gender: Gender.t;
    }

  let add_client first last age gender_str =
    {
      first_name = first;
      last_name = last;
      age = age;
      gender =
        try
          Gender.of_string gender_str
        with
          Invalid_argument _ -> raise WrongClientInformation
    }

  let compare (c1:t) (c2:t) : int =
    match (compare c1.last_name c2.last_name) with
      0 ->
        begin
          match (compare c1.first_name c2.first_name) with
            0 ->
              if Gender.compare c1.gender c2.gender = 0 then
                0
              else compare c1.age c2.age
            | i -> i
        end
      | i -> i

  let to_string c =
    "Client " ^ c.first_name ^ " " ^ c.last_name ^ " is a "
    ^ (Gender.to_string c.gender) ^ " aged " ^ (string_of_int c.age) ^ "."

  let wish_happy_birthday c =
    let () = print_endline ("Happy birthday, " ^ c.first_name ^ "!") in
    c.age <- c.age + 1

end